package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Parsing {

    public List<Day> read(String path) {
        try {
            File file = new File(path);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            return parseDate(bufferedReader.lines().collect(Collectors.toList()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<String> findData(List<Day> days) {
        double averageAirTemperature = 0;
        double averageHumidity = 0;
        double averageWindSpeed = 0;
        double highestTemperature = Double.MIN_VALUE;
        String dayAndHourWithHighestTemperature = null;
        String dayAndHourWithLowestHumidity = null;
        String dayAndHourWithStrongestWind = null;
        double lowestHumidity = Double.MAX_VALUE;
        double strongestWind = -1;
        int[] countOfWindDirection = new int[8];
        Arrays.fill(countOfWindDirection, 0);

        for (Day day : days) {

            if (day.getTemperature() > highestTemperature) {
                highestTemperature = day.getTemperature();
                dayAndHourWithHighestTemperature = day.getDate();
            }
            if (day.getHumidity() < lowestHumidity) {
                lowestHumidity = day.getHumidity();
                dayAndHourWithLowestHumidity = day.getDate();
            }

            if (day.getWindSpeed() > strongestWind) {
                strongestWind = day.getWindSpeed();
                dayAndHourWithStrongestWind = day.getDate();
            }

            defineWindDirection(countOfWindDirection, day.getWindDirection());

            averageAirTemperature += +day.getTemperature();
            averageHumidity += day.getHumidity();
            averageWindSpeed += day.getWindSpeed();
        }
        ArrayList<String> verifiedData = new ArrayList<>();
        verifiedData.add("Средняя температура воздуха " + averageAirTemperature / days.size() + "℃");
        verifiedData.add("Средняя влажность " + averageHumidity / days.size() + "%");
        verifiedData.add("Средняя скорость ветра " + averageWindSpeed / days.size() + "км/ч");
        verifiedData.add(parseDate(dayAndHourWithHighestTemperature) + " температура достигла " + highestTemperature + "℃");
        verifiedData.add("Cамая низкая влажность " + lowestHumidity + "% " + parseDate(dayAndHourWithLowestHumidity));
        verifiedData.add("Cамый сильный ветер " + strongestWind + "км/ч " + parseDate(dayAndHourWithStrongestWind) );
        verifiedData.add("Самое частое направление ветра (по сторонам света) - " + findMostFrequentWindDirection(countOfWindDirection));
        return verifiedData;
    }

    public String findMostFrequentWindDirection(int[] countOfWindDirection) {
        String mostFrequentWindDirection = null;
        int mostFrequentWindDirectionIndex = 0;
        int maxElem = -1;
        for (int i = 0; i < 8; i++) {
            if (countOfWindDirection[i] > maxElem) {
                mostFrequentWindDirectionIndex = i;
                maxElem = countOfWindDirection[i];
            }
        }

        switch (mostFrequentWindDirectionIndex) {
            case (0):
                mostFrequentWindDirection = "Север";
                break;
            case (1):
                mostFrequentWindDirection = "Северо-восток";
                break;
            case (2):
                mostFrequentWindDirection = "Восток";
                break;
            case (3):
                mostFrequentWindDirection = "Юго-восток";
                break;
            case (4):
                mostFrequentWindDirection = "Юг";
                break;
            case (5):
                mostFrequentWindDirection = "Юго-запад";
                break;
            case (6):
                mostFrequentWindDirection = "Запад";
                break;
            case (7):
                mostFrequentWindDirection = "Северо-запад";
                break;
        }
        return mostFrequentWindDirection;
    }

    public void defineWindDirection(int[] countOfWindDirection, double windDirection) {
        if (windDirection < 22.5 && windDirection >= 337.5) {
            countOfWindDirection[0]++;
        } else if (windDirection >= 22.5 && windDirection < 67.5) {
            countOfWindDirection[1]++;
        } else if (windDirection >= 67.5 && windDirection < 112.5) {
            countOfWindDirection[2]++;
        } else if (windDirection >= 112.5 && windDirection < 157.5) {
            countOfWindDirection[3]++;
        } else if (windDirection >= 157.5 && windDirection < 202.5) {
            countOfWindDirection[4]++;
        } else if (windDirection >= 202.5 && windDirection < 247.5) {
            countOfWindDirection[5]++;
        } else if (windDirection >= 247.5 && windDirection < 292.5) {
            countOfWindDirection[6]++;
        } else if (windDirection >= 292.5 && windDirection < 337.5) {
            countOfWindDirection[7]++;
        }

    }

    public String parseDate(String s) {
        return (s.substring(6, 8) + "."
                + s.substring(4, 6) + "." + s.substring(0, 4) + " в " + s.substring(9, 11) + ":" +
                s.substring(11, 13));
    }

    public List<Day> parseDate(List<String> list) {
        List<Day> days = new ArrayList<>();
        for (int i = 10; i < list.size(); i++) {
            String[] string = list.get(i).split(",");
            days.add(new Day(string[0], Double.parseDouble(string[1]), Double.parseDouble(string[2]), Double.parseDouble(string[3]), Double.parseDouble(string[4])));
        }
        return days;
    }

    public void write(ArrayList<String> verifiedData) {
        try {
            File file = new File("Data\\", "Data.txt");
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for (String data :
                    verifiedData) {
                bufferedWriter.write(data + "\n");
            }
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
