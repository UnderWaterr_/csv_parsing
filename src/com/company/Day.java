package com.company;

public class Day {

    private final String date;
    private final double temperature;
    private final double humidity;
    private final double windSpeed;
    private final double windDirection;

    public Day(String date, double temperature, double humidity, double windSpeed, double windDirection) {
        this.date = date;
        this.temperature = temperature;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
    }

    public String getDate() {
        return date;
    }

    public double getTemperature() {
        return temperature;
    }

    public double getHumidity() {
        return humidity;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public double getWindDirection() {
        return windDirection;
    }
}
