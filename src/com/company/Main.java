package com.company;


import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Parsing parsing = new Parsing();
        parsing.write(parsing.findData(parsing.read(scanner.nextLine())));
    }
}